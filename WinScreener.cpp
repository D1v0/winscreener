#include <windows.h>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include <iostream>
#include <gdiplus.h>
//#include <gdiplus/GdiPlusImageCodec.h>
#include <time.h>
#include <tlhelp32.h>

using namespace std;

map <HWND, wstring> topLevelWindowsN;
map <HWND, wstring> topLevelWindows;
map <HWND, wstring> childlWindows;
vector <DWORD> threadList;
DWORD processID;

std::string to_utf8 (const wchar_t* buffer, int len)
{
    int nChars = ::WideCharToMultiByte (CP_UTF8,
                                        0,
                                        buffer,
                                        len,
                                        NULL,
                                        0,
                                        NULL,
                                        NULL);
    if (nChars == 0)
        return "";

    string newbuffer;
    newbuffer.resize (nChars);
    ::WideCharToMultiByte (CP_UTF8,
                           0,
                           buffer,
                           len,
                           const_cast <char*> (newbuffer.c_str ()),
                           nChars,
                           NULL,
                           NULL);

    return newbuffer;
}

std::string to_utf8 (const std::wstring& str)
{
    return to_utf8 (str.c_str (),
                    (int) str.size ());
}

string toUTF8 (wstring strOut)
{
    char pcOut [65535];
    WideCharToMultiByte (CP_UTF8,
                         0,
                         strOut.data (),
                         -1,
                         pcOut,
                         65535,
                         0,
                         0);
    return string (pcOut);
}

wstring toUCS2 (string kpcIn)
{
    WCHAR pcOut [65535];
    int size = sizeof(pcOut) / sizeof(pcOut [0]);
    MultiByteToWideChar (CP_UTF8,
                         0,
                         kpcIn.data (),
                         -1,
                         pcOut,
                         size);
    return wstring (pcOut);
}

BOOL CALLBACK EnumWindows (HWND hwnd, LPARAM lp)
{
    ofstream file;
    DWORD pid = (DWORD) lp;
    DWORD windowThreadID;
    GetWindowThreadProcessId (hwnd,
                              &windowThreadID);
    /* if (windowThreadID == GetCurrentThreadId ())
     return TRUE;*/
    wchar_t buf [255];
    char cName [255];
	if (pid == windowThreadID)
	{
		GetClassNameA(hwnd,
			cName,
			255);
		string className(cName);
		if (className == "#32770")
		{
			//cout << "className = " << className << endl;
			LONG style = GetWindowLongPtr(hwnd,
				GWL_STYLE);
			if (/*(style & WS_CAPTION) &&*/ (style & WS_VISIBLE))
			{
				//cout << "PID = " << pid << endl;
				GetWindowText(hwnd,
					buf,
					255);
				topLevelWindows[hwnd] = wstring(buf); //wstring (buf);
				//cout << cout.hex << hwnd << endl;
			}
		}
	}
    return TRUE;
}

BOOL CALLBACK EnumChildWindows (HWND hwnd, LPARAM lp)
{
    wchar_t buf [255];
    LONG style = GetWindowLongPtr (hwnd,
                                   GWL_STYLE);
    if ((style & WS_CAPTION) && (style & WS_VISIBLE))
    {
        GetWindowText (hwnd,
                       buf,
                       255);
        childlWindows [hwnd] = wstring (buf);
        cout << hwnd << cout.hex << endl;
    }
    return TRUE;
}

static int GetEncoderClsid (const WCHAR* format, CLSID* pClsid)
{
    UINT num = 0; // number of image encoders
    UINT size = 0; // size of the image encoder array in bytes

    Gdiplus::ImageCodecInfo* pImageCodecInfo = NULL;

    Gdiplus::GetImageEncodersSize (&num,
                                   &size);
    if (size == 0)
    {
        return -1;
    }
    pImageCodecInfo = new Gdiplus::ImageCodecInfo [size];
    if (pImageCodecInfo == NULL)
    {
        return -1;
    }
    GetImageEncoders (num,
                      size,
                      pImageCodecInfo);

    for (UINT j = 0; j < num; ++j)
    {
        if (wcscmp (pImageCodecInfo [j].MimeType,
                    format) == 0)
        {
            *pClsid = pImageCodecInfo [j].Clsid;
            delete [] pImageCodecInfo;
            return j;
        }
    }

    delete [] pImageCodecInfo;
    return -1;
}

void saveScreenShot (HBITMAP hBmp, LPCWSTR lpszFilename, ULONG uQuality)
{
    Gdiplus::GpBitmap* pBitmap;
    Gdiplus::DllExports::GdipCreateBitmapFromHBITMAP (hBmp,
                                                      NULL,
                                                      &pBitmap);
    CLSID imageCLSID;

    GetEncoderClsid (L"image/jpeg",
                     &imageCLSID);
    Gdiplus::EncoderParameters encoderParams;
    encoderParams.Count = 1;
    encoderParams.Parameter [0].NumberOfValues = 1;
    encoderParams.Parameter [0].Guid = Gdiplus::EncoderQuality;
    encoderParams.Parameter [0].Type = Gdiplus::EncoderParameterValueTypeLong;
    encoderParams.Parameter [0].Value = &uQuality;

    Gdiplus::DllExports::GdipSaveImageToFile (pBitmap,
                                              lpszFilename,
                                              &imageCLSID,
                                              &encoderParams);
   // cout << "get " << GetLastError () << endl;
}

const wstring currentDateTime ()
{
    time_t now = time (0);
    tm tstruct;
    wchar_t buf [80];
    tstruct = *localtime (&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    wcsftime (buf,
              sizeof(buf),
              L"%d.%m.%Y_%H.%M.%S",
              &tstruct);

    return buf;
}

void CALLBACK Timer (HWND hwnd, UINT msg, UINT_PTR event, DWORD time)
{
    topLevelWindows.clear ();
    ::EnumWindows (EnumWindows, (LPARAM) processID);

    for (auto window = topLevelWindows.begin ();
            window != topLevelWindows.end (); window++)
    {
        cout << "Done" << endl;
        HDC dc = GetDC (window->first);
        RECT rect;
        GetClientRect (window->first,
                       &rect);
        //cout << "rect.right - rect.left = " << rect.right - rect.left << endl;
        //cout << "rect.bottom - rect.top = " << rect.bottom - rect.top << endl;
        HBITMAP bmp = CreateCompatibleBitmap (dc,
                                              rect.right - rect.left,
                                              rect.bottom - rect.top);
        HDC memDc = CreateCompatibleDC (dc);
        SelectObject (memDc,
                      bmp);
      
        PrintWindow (window->first,
                     memDc,
                     1);

        wstring sfilename = window->second;

		if (sfilename.find(L"$") != std::wstring::npos)
			sfilename.replace(sfilename.find(L"$"), 1, L"S");

		if (sfilename.find(L":") != std::wstring::npos)
			sfilename.replace(sfilename.find(L":"), 1, L" ");

		if (sfilename.find(L"`") != std::wstring::npos)
			sfilename.replace(sfilename.find(L"`"), 1, L" ");

		if (sfilename.find(L"'") != std::wstring::npos)
			sfilename.replace(sfilename.find(L"'"), 1, L" ");

		if (sfilename.find(L"€") != std::wstring::npos)
			sfilename.replace(sfilename.find(L"€"), 1, L"E");

		if (sfilename.find(L"/") != std::wstring::npos)
			sfilename.replace(sfilename.find(L"/"), 1, L"T");

       // for (int i = 0; i < sfilename.size (); i++)
       // {
		//	wchar_t c = sfilename[i];
		//	sfilename.replace(sfilename.find_first_of(L"$"), 1, L"S");
           /* if (sfilename [i] == L'$')
                sfilename [i] = L'S';*/
		//	if (sfilename[i] == L':')
		//		sfilename[i] = L' ';
        //    if (sfilename [i] == L'/')
        //        sfilename [i] = L'T';
       //     if (sfilename [i] == L'€')
       //         sfilename [i] = L'E';
            /*if (sfilename [i] == L' ')
             sfilename [i] = L'_';
             if (sfilename [i] == L',')
             sfilename [i] = L'_';*/

    //    }
        /*string s = toUTF8 (sfilename);
         string ss = s.substr (0,
         s.find_last_of ("-"));
         ss += currentDateTime ();
         ss += ".jpg";
         cout << "ss = " << ss << endl;*/
        wstring fileName = sfilename;    //toUCS2 (ss);
        fileName += L"_";
        fileName += currentDateTime ();
        fileName += L".jpg";
        wcout << L"fileName" << fileName << endl;
        saveScreenShot (bmp,
                        fileName.c_str (),
                        100);
        ReleaseDC (window->first,
                   dc);
        DeleteObject (bmp);
        DeleteObject (memDc);
        /*ShowWindow (window->first,
         SW_FORCEMINIMIZE);*/

    }
}

LRESULT CALLBACK WndProc (HWND hwnd, UINT msg, WPARAM wp, LPARAM lp)
{
    if (msg == WM_DESTROY)
    {
        PostQuitMessage (0);
        return 0;
    }
    return DefWindowProc (hwnd,
                          msg,
                          wp,
                          lp);
}

void FindWindows (DWORD dwProcessID)
{
    HWND hwnd = NULL;
    wchar_t buf [255];
    do
    {
        hwnd = GetWindow (GetDesktopWindow (),
                          GW_HWNDNEXT);
        DWORD dwPID = 0;
        GetWindowThreadProcessId (hwnd,
                                  &dwPID);
        cout << "dwPID = " << dwPID << endl;
        if (dwPID == dwProcessID)
            GetWindowText (hwnd,
                           buf,
                           255);
        topLevelWindows [hwnd] = wstring (buf);
    } while (hwnd != NULL);
}

BOOL ListProcessThreads (DWORD dwOwnerPID)
{
    HANDLE hThreadSnap = INVALID_HANDLE_VALUE;
    THREADENTRY32 te32;

    // Take a snapshot of all running threads
    hThreadSnap = CreateToolhelp32Snapshot (TH32CS_SNAPTHREAD,
                                            0);
    if (hThreadSnap == INVALID_HANDLE_VALUE)
        return ( FALSE);

    // Fill in the size of the structure before using it.
    te32.dwSize = sizeof(THREADENTRY32);

    // Retrieve information about the first thread,
    // and exit if unsuccessful
    if (!Thread32First (hThreadSnap,
                        &te32))
    {
        //printError ( TEXT("Thread32First"));  // Show cause of failure
        CloseHandle (hThreadSnap);     // Must clean up the snapshot object!
        return ( FALSE);
    }

    // Now walk the thread list of the system,
    // and display information about each thread
    // associated with the specified process
    do
    {
        if (te32.th32OwnerProcessID == dwOwnerPID)
        {
            threadList.push_back (te32.th32ThreadID);
        }
    } while (Thread32Next (hThreadSnap,
                           &te32));

    //_tprintf ( TEXT("\n"));

//  Don't forget to clean up the snapshot object.
    CloseHandle (hThreadSnap);
    return ( TRUE);
}

int main ()
{
    ifstream pidFile;
    pidFile.open ("pid.txt");

    Gdiplus::GdiplusStartupInput gdiplusStartupInput;
    ULONG_PTR gdiplusToken;
    Gdiplus::GdiplusStartup (&gdiplusToken,
                             &gdiplusStartupInput,
                             NULL);
    pidFile >> processID;

    cout << "processID = " << processID << endl;

    SetTimer (NULL,
              0,
              1000,
              Timer);

    MSG msg;
    while (GetMessage (&msg,
                       NULL,
                       0,
                       0))
    {
        TranslateMessage (&msg);
        DispatchMessage (&msg);
    }
    return 0;
}
